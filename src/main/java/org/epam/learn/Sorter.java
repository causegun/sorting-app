package org.epam.learn;

import java.util.Arrays;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.sort;

public class Sorter {
    /**
     * Sorts input String array as int array
     *
     * @param args the String array to sort.
     */
    public int[] sortArray(String[] args) {
        if (args == null) {
            throw new IllegalArgumentException("input should not be empty");
        }
        if (args.length > 10) throw new IllegalArgumentException("input should contain up to ten arguments");
        if (args.length == 0) throw new IllegalArgumentException("input should contain at least one argument");
        int[] sortedArray = new int[]{};
        for (String arg : args) {
            sortedArray = Arrays.copyOf(sortedArray, sortedArray.length + 1);
            sortedArray[sortedArray.length - 1] = parseInt(arg);
        }
        sort(sortedArray);
        return sortedArray;
    }
}
