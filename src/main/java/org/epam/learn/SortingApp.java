package org.epam.learn;

import org.apache.log4j.Logger;

import java.util.Arrays;


public class SortingApp {
    static Logger log = Logger.getLogger(SortingApp.class.getName());

    public static void main(String[] args) {
        log.info("input arguments: " + Arrays.toString(args));
        Sorter sorter = new Sorter();
        int[] sortedArray = sorter.sortArray(args);
        log.info("output array: " + Arrays.toString(sortedArray));
        System.out.println(Arrays.toString(sortedArray));
    }
}
