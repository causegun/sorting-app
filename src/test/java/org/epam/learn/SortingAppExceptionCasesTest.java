package org.epam.learn;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SortingAppExceptionCasesTest {
    Sorter sorter = new Sorter();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void nullCaseTest() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("input should not be empty");
        sorter.sortArray(null);
    }

    @Test
    public void zeroArgsCaseTest() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("input should contain at least one argument");
        sorter.sortArray(new String[]{});
    }

    @Test
    public void moreThanTenArgsCaseTest() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("input should contain up to ten arguments");
        sorter.sortArray(new String[11]);
    }
}
