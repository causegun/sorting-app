package org.epam.learn;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class SortingAppDefaultCasesTest {
    private final String[] argsArray;
    private final int[] expectedArray;

    Sorter sorter = new Sorter();

    public SortingAppDefaultCasesTest(String[] argsArray, int[] expectedArray) {
        this.argsArray = argsArray;
        this.expectedArray = expectedArray;
    }

    @Parameterized.Parameters
    public static List<Object[]> inputParameters() {
        String[] tenArgsArray = new String[]{"1", "3", "6", "-2", "23", "2023", "2022", "12", "-143", "10"};
        int[] tenArgsExpectedOutputArray = new int[]{-143, -2, 1, 3, 6, 10, 12, 23, 2022, 2023};
        String[] nineArgsArray = new String[]{"31", "26", "23", "34", "-23", "48", "37", "25", "49"};
        int[] nineArgsExpectedOutputArray = new int[]{-23, 23, 25, 26, 31, 34, 37, 48, 49};
        String[] oneArgArray = new String[]{"55"};
        int[] oneArgExpectedOutputArray = new int[]{55};
        return Arrays.asList(new Object[][]{
                {tenArgsArray, tenArgsExpectedOutputArray},
                {nineArgsArray, nineArgsExpectedOutputArray},
                {oneArgArray, oneArgExpectedOutputArray}
        });
    }

    @Test
    public void defaultCaseTest() {
        Assert.assertArrayEquals(sorter.sortArray(argsArray), expectedArray);
    }
}
